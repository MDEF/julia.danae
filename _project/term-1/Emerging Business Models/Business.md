---
title: Emerging Business Models
period: 16 May 2019
date: 2018-12-17 12:00:00
term: 1
published: true
---

<span class="text-small">*Introduced by Javier Creus and Valeria Righi from Ideas For Change*</span>

> *"Don't try to change the system, build another one that will make the first one obsolete"* Buckminster Fuller

## Notes and reflections
Trying to understand how society is changing.

IMPACT AND ECOSYSTEMS: an expanded ecosystem.
How to create a sustainability strategy for the project and how to maximize impact?
- Define impact variables
- Analyse previous cases
- Re-analyse idea

What innovations have had the most impact in different sectors?

- People
- Knowledge (academic and non-academic)
- Spaces (homes, humanitarian emergencies)
- Funding (crowdfunding, philanthropic capital, social impact bonds)
- Dissemination (open, communities)
- Value creation (social system, communities)

At what level would I like to impact the world?

Impact levels:
 Direct service (immediate need attended) > many direct services (regional level) > systemic change (addressing a root cause of a social problem) > change of paradigm (changing the way society thinks about an issue).

Design strategies depend on those levels. From service to capacity to knowledge, movement and behaviour change to infrastructure to legislation, all are type of solutions at different levels.

Idea of social adoption is key to the speed of growth. Empower users and facilitate adoption. Focus on most important and critical steps (like a checklist).
The idea of 'systemic innovation' and 'systemic innovation'.

Need to understand the logic of a system to make change.

What do you want to change in reality? What are the elements that produce impact?

Importance of building on competing products.

Innovation is about the combinations of the same old ingredients. ELEMENTS X RULES X AGENTS is the equation for the field of possibilities.
The more people you have thinking, the more possibilities to generate solutions. What is the universe of things I can combine?

What do ne innovations make abundant and what do they make scarce?
Technology is what makes element connectable. In society, this connectivity is directed by trust.

The idea of 'data keepers' and 'data users'.

What technologies rule our lives today?

We were introduced to the Pentagrowth Model (2015, Ideas for Change) with five key branches: connect, collect, empower, enable and share.

The more capacities and more roles you ca give to users the better value. If you don't use the knowledge it becomes a liability. Better to share the knowledge than to keep it for yourself in order to enter the game of the market. Sharing is managing differently. And if you suceed to engage, you manage to sustain out of commercial rules.

Need to reinvent according to new network. Think of what is already available to build on. Go through the whole value chain to see where you can use every type of assets.


## Session 1
The first assignment consist in finding a way to describe our project, find innovations in relation to my project and revisit the change my project is aiming to do.

<object data="{{site.baseurl}}/resources/Session1-business-model.pdf" type="application/pdf" width="60%" height="720">
  <p>Alternative text - include a link <a href="{{site.baseurl}}/resources/Session1-business-model.pdf">to the PDF!</a></p>
</object>


## Session 2
We started the session with a recap of the first session assignment.

<object data="{{site.baseurl}}/resources/Session2-3-business-model.pdf" type="application/pdf" width="60%" height="720">
  <p>Alternative text - include a link <a href="{{site.baseurl}}/resources/Session2-3-business-model.pdf">to the PDF!</a></p>
</object>
