---
title: Research Trip | Shenzhen
period: 07-15 January 2019
date: 2018-12-17 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/P1094053.JPG)
China Research Trip, January 2019

## Made in China, consumed by the world

In the last 40 years, Shenzhen's population has grown from 30000 people to 12.53 million people (2017), shifting the market from fishing to electronics, transforming radically the rhythm of the city.
The Pearl river delta flows into the south China sea. It is one of the most densely urbanized regions in the world, making Shenzhen an "emerging mega city". Shekou harbour, literally translated "snake's mouth", is the world's third busiest shipping port, after Shanghai and Singapore, with 24 million metric tones of container cargo leaving the port every month with electronics.
Shenzhen's technological ecosystem relies heavily on the existence of free trade and grey-market labour. Huanqiangbei is a major electronics manufacturing hub in the sub district of Futian, also called the "Silicon Valley of Hardware".
This city is carrying a dual cultural DNA; made in China, consumed by the world.

![]({{site.baseurl}}/P1114249.JPG)

### Local ecosystem
##### Shenzhen *Sudu*
- [ ] *ADD Interactive map with pin of places we went to visit (factories, studios)*

"*Sudu*" means the speed at which the city is working. In Shenzhen, *Shanzhai* is using that speed for the development and distribution of counterfeit consumer goods (imitation and trademarks), which is creating  an environment developing "hardware memes". We've learned the usual rhythm of the local ecosystem for production goes from 4 to 6 weeks to design a product and put it on the market. In comparison with an average of a year in western companies, it is a very competitive and fast changing environment for product manufacturing. On top of the hardware, software reinforce this speed of consumption with the use of payment platforms such as AliPay and WeChat to make quick effortless payments for anything from a temporary street food stand to a shop as part of a massive commercial building.

##### Shenzhen fearless attitude to test
"Try one thing one day, if it doesn't work, move to something else the next." is the way of thinking of all street markets in Shenzhen. Technology is exponential but humans are linear. Communities have the ability to grow exponentially.
> "Many small people, in small places, doing small things, **(connected)** can change the world." Eduardo Galeano

##### Cost of living
![]({{site.baseurl}}/P1144407.JPG)
Urban Village in Shenzhen introduced by Jason Hilgefort
Pollution (air quality), overcrowding, poor working conditions.
- One child policy as a form of pressure from the government with aging population in a rapid manufacture of hardware. *(two years ago) Look up for government motivation for recent change?*
- Rural to urban *(newspaper)*

### Visits
##### SZOIL
Maker space Shenzhen Open Innovation Lab connects makers with design firms (such as Innozen).
Why have a fablab in Shenzhen ecosystem when everything is already so easy to make? Shenzhen is itself a gigantic fablab set up by the government as an experiment.
So what can you do in a fablab? Make things people actually need.
No need for pure innovation, but rather restore past glory such as cars into electric cars, for better efficiency and sustainability.

**David Li**
The question today for Shenzhen is changing from who gets to innovate, towards what does it take for a place to innovate. Access to technology, knowledge (internet, open source software, cloud services), and products. Shenzhen is democratizing those three things. Maker movement is now open hardware, internet sharing and digital fabrication. What's the next step?
Deng Xiaoping,  argued that "poverty is not socialism" and encouraged the creation of a market economy and capitalist-like enterprises. By the early 1990s his reforms had helped lift an estimated 170 million peasants out of extreme poverty.
What is driving Shenzhen to innovate is the fight to stand out of all the makers / businesses. Not only copying. Small variations. If it is only a copy, it doesn't work as a symbol anymore.
Here are some examples of smaller brands who started by selecting a "white label" mobile phone in Huaquiangbei and went to see the producer to launch the project.

Examples:
Techno (mobile phone with African costumers)
Wiko (3% european market)
Nerve (mobile phone company in Nigeria)

**Technologies are commodities.**
Example - Coffee beans. If you want to open a coffee shop you don't need to know how to grow a coffee tree.
E-commerce in a northern Chinese village (and before that it was waste recycle.) Taobao villages is one example amongst 2800 others in China. Today e-commerce has 10 billion revenue  with a master plan.
Example: after change in the one child law policy, bunk beds are famous in that e-commerce.

**Global resources, local problems**
- where is the focus? what to do?
- Dynamism in Africa (ex. Nairobi)
Local cooperative in Europe are not scalable, not exporting to China those kind of products.
The difference is location.
Mental quack for "*uniqueness*" in Europe and most western countries.

**Leverage global, think local**.

##### SEEED STUDIO
Founded in 2008, Seeeduino was released.
2010, Grove system - an open connector standard and open source libraries for prototyping.
"No open environment" in Shenzhen for good engineers to decide to do something.
Maker movement recognised in China mainstream media from 2013.
Rephone (2014) is the 1st open source modular phone kit
Problem: there's more maker space than makers.

![]({{site.baseurl}}/P1114227.jpeg)

##### X.FACTORY
**Eric Pan**
IOT hardware innovation lab
There's a gap between IT and IOT that needs to be closed. Strategic partnership with digital leaders is made as a first step towards making supplying chain but also insuring the quality is maintained.

**Innovation with China**
China has the supply chain put in place (Shenzhen specifically), it has the manufacturing skills to get a production started. Seeed is looking into changing the mass manufacturing and shipping process into something more sustainable.

**Move from Shanzhai to open technologies.**
What can be the most natural interaction using IOT? IOT is deleting the process to get to something, suppressing the experience of doing something to get something.
Makers need to focus on deployment. Eric Pan's belief is somehow shared with the purpose of the MDEF master... Makers here are able to scale any possible manufactured project to get in industries.
Examples: Make Bay "Little Detective" project, "IOTea - Internet of Tea" , a monitoring system for tea cultivation and production.
Three places:
- Smart Manufacturing, Dongguan
- Smart Building, Shenzhen
- Ecological Technology, Hebei (Hebei a city concerned with heavy pollution).

Decline and difficulties seen in Automobile and Real Estate in recent years shows an interest in renovation rather than keeping the pace in producing the same things over and over again.
An innovation trigger in Shenzhen. However, there seems to be missing designers, people do not know what to produce anymore. They have the capacities and the manufacturing ecosystem but no ideas on what to make next. An industry based on manufacturing needs more design ideas and collaboration opportunities could potentially develop from this need.
Would Chinese factories be open to more experimentation in the use of new materials that would probably beget a change in the production system too?

**Design from manufacturers, not for manufacturers.**
The adopted systematic approach of manufacturing here seems to be defining a one way only communication between makers and manufacturers. According to Eric Pan, makers need to design things that fit in the current manufacturing system and not the other way around.
First step, to start manufacturing a project, would be to come to Shenzhen to have a first look at the manufacturing possibilities and establish an understanding of what is possible.

**How to go from centralised to distributed?**
Go smaller for manufacture equipment.
A lot of Chinese small companies are moving out, closer to their Western costumers, with a more intersting competition.

### Question of sustainability
​At the end of the week we presented as groups of interest our findings in the context of Shenzhen and how it could better inform our individual projects. Our group was looking into biology, which was challenging in this context. We approached this interest with personal car, business sustainability and craftsmanship.   

##### Business sustainability

![]({{site.baseurl}}/ShenzhenPresentationDocumentation4.jpeg)

- **Green spaces as a commodity**
Primarily as recreational spaces/land.
Vast and varied use of public parks - chess, cards, music, song, dance, ...
Parks as people's gardens - respected, used.

- **Organic and artificial**  
Valuing the aesthetics and appearances over the nurture of living plants  - replication and imitation of plant life
Used to be done for artistic reinterpretation of nature and not a replicate of what is nature.
​Trend for synthetic plant - plastic plants require resources (usually oil-based polymers and energy) to make. So do organic plants but with benefits (healthy, able to be recycled, supporting local horticultural industry,...) at the end.

- **Green as an economic motivator**
Gap between designer, maker and consumer.
*AliPay* is using environmental issues to reinforce the gap between the actions and their consequences. *Example: Alipay online payment app.*
A problem when coming to design sustainable physical objects.

​Linking ecological sustainability with business sustainability to work towards a more sustainable sensitive future.
​http://green.ust.hk/en-us/about
After reading a newspaper in Hong Kong about Rural Sustainability, I looked further into the idea of a rural and urban symbiosis - natural craftmanship, rural appropriate technologies and design, developing new businesses in a sustainable frame.
http://www.socsc.hku.hk/psl/
​Lai Chi  Wo - Rural revitalisation

##### Crafting in manufacture
- Work ethics
Process within the context of factories
The 10% of the manufacture process is the part that has the potential to create a stronger relationship with the making and the outcomes.
- Attitude and cultural values
Inherited over generations, tradition
- Hone your craft
Honouring the crafting process (focus of interaction, dedication)

**Short film made by our research team on site [HERE](https://vimeo.com/313585394/404beccca9)**.

##### Potential ways forward
- Shift focus away from short term gains towards long term goals focused on sustainability and well-being
- Embed attitudes and values from crafts in manufacturing processes
- Enhance traditional crafts with networked technologies to scale up production in sustainable ways
- Reconnect people with end products and how they are made (*Example: IOTea*)

#### 	Question

![]({{site.baseurl}}/ShenzhenPresentationDocumentation7.jpg)

**How can craftmanship foster a symbiosis between technology and biological systems?**



---------------------

## References

[Seeed Studio · GitHub](https://github.com/Seeed-Studio)

[Seeed Studio blog](https://www.seeedstudio.com/blog/2019/01/16/inside-made-in-china-dongguan-factories-welcome-fablab-barcelona/)

[Grove - Seeed Studio](https://www.seeedstudio.com/grove.html)

[xfactory - 柴火创客空间](http://www.chaihuo.org/xfactory/)

[Shenzhen Open Innovation Lab – 深圳开放创新实验室](https://www.szoil.org/)

[Shenzhen Xunjiexing Technology Corp. Ltd](http://en.jxpcb.com/p_about.htm)

[Shenzhen Strongd Model Technology Co., Ltd.](https://strongd.en.made-in-china.com/)

http://edition.cnn.com/2009/WORLD/asiapcf/08/10/china.zhuo.mourned/index.html

https://www.bworldonline.com/huawei-eyes-smartphone-lead-after-passing-apple/

https://www.wired.co.uk/article/how-china-became-tech-superpower-took-over-the-west

https://www.technologyreview.com/s/612571/inside-shenzhens-race-to-outdo-silicon-valley/?fbclid=IwAR3t0EEmoeTu6y2SIcMzIPDANf_WfAkNlFlmUnT8ci7M3iV2BzhH-7mrIxk

[Why Urban Villages Are Essential to Shenzhen's Identity - Sixth Tone](http://www.sixthtone.com/news/1002091/why-urban-villages-are-essential-to-shenzhens-identity)

'Makers: The New Industrial Revolution' by Chris Anderson

Global Handsets Shipments Market Share Q4 2017 for Shenzhen (showing percentage of production for each cell brands).

'The Innovator's Dilemma' by Clayton M. Christensen

[Theory of disruptive innovation](https://hbr.org/2015/12/what-is-disruptive-innovation)

[Death by Design](https://greenpeacefilmfestival.org/en/film/death-by-design/)

MOMO Wall

Bimernet - Online access to architectural models to measure and make change.

Cube Design

Sisbot - magnet drawing in sand

https://www.chinahighlights.com/travelguide/article-park-life.html

https://heyhorti.com/blogs/thedirt/whats-so-bad-about-artificial-plants
https://jeffollerton.wordpress.com/2017/01/20/when-did-plastic-plants-become-acceptable/

​Examples of observed trends and innovations in China:
​- Tree antenna, Kaal Masten B.V.*
- ​http://www.astronomy.com/news/2018/10/china-artificial-moons
​- https://www.indiatoday.in/fyi/story/chinese-fashion-trend-people-are-growing-plants-on-their-heads-267552-2015-10-11
- ​https://www.digitaltrends.com/cool-tech/china-first-plant-on-moon/
