---
title: 11 | From Bits to Atoms
period: 10-16 December 2018
date: 2018-12-10 12:00:00
term: 1
published: true
---

*Introduced by Santi Fuentemilla and Xavi Dominguez.*

![]({{site.baseurl}}/PollockBotDrawingOne.jpg)

## Fab Academy introduction

Fab Academy

*http://archive.fabacademy.org/archives/2017/master/*

Class created "How to Make Almost Anything" by Neil Gershenfeld.

TRained for the essential manufacturing skills in the industry.

Opportunity to produce something you never knew how to.

There's a list of machines that are needed to become a fablab past of the network, It wll assure that a project done in a fablab can be done in any other fablab.

Fab Academy as a distributed educational system.

Machines as tools to produce an outcome.

You don't need all the knowledge to start doing, this will be gained with the weeks.

LEARN + MAKE + SHARE

Weekly classes with Neil Gershenferld (MIT) by videoconferencing system + hands on with local instructor guidance to complete projects + documents the experience through a portfolio process (video, one slide visual).

Three evaluation system:

- student self-assessment
- local instructors
- global evaluation


## Drawing Machine

![]({{site.baseurl}}/week-11/P1011118.jpg)

*https://bitsandatoms.gitlab.io/site/*

My continuous interest in growth and visual communication has made me want to use this week to create a parametric drawing machine. Initially I imagined to create a parametric design on Grasshopper to then try translate this movement created by the variable on the computer, to show the drawn result in live and physically.



### ‘PollockBot’

![]({{site.baseurl}}/week-11/PC143672.jpg)

*https://mdef.gitlab.io/animal-farm/*

As a group we decided to create a swarm of drawing robots that behave within a specific enclosed environment and affect their behaviour with light and control the tools to create the outputs. We created an input with the vibrations and an output with the drawings.

As we started to design the drawing machine, I took charge of testing the it's vibration in comparison with the drawing result, after every change made. We  tested different tools for drawing and finding the small materials to complete the robot. I then helped to create the frame in which the machine drew. I learned to use design and prepare the files for laser cutting. I filmed and photographed everything we've done during the week. And each of us have written a day of the week for the documentation of the work.



------

<https://additivism.org/>

<https://vimeo.com/122642166>

http://archive.fabacademy.org/archives
