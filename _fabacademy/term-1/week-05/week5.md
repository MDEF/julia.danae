---
title: 5 | 3D Scanning and Printing
period: 20 February 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---


### 3D scanning

### 1
![]({{site.baseurl}}/3dscanning4.jpeg)

I scanned a stool as an exercise to understand what the conditions are to use this Skanect software and Kinect camera. I first tried with a climbing shoe but the object was too small for the software to be able to scan properly. I placed the object in a position giving me enough distance from the object and be able to access it 360 degrees for the scanning.

![]({{site.baseurl}}/3dscanning3.jpeg)
![]({{site.baseurl}}/3dscanning1.jpeg)

The settings are straight forward and only a few steps long. I first set up the scene as object, then select bounding box (which indicates the distance needed to scan the object, it will appear as a light green if it is the perfect condition to scan), and finally in process stage, I have put the strategy option under watertight so that the filling is completed and makes it one entity (it can also smooth the surface).

### 2
I tried a second time to scan an object as I did not save the file of the scan the first time. I did it this time with Maite to be fast.

First, I opened SKANECT software, select *New* > *Scene* Object Bounding > *Box* 1.5x1.5 meters > *Aspect ratio* Normal > *Path* Desktop > *Config files* none and press *START*.

Problems: The camera is not working - Off. Pic1. We took another camera. We put the same parameters, click *Start* and it’s still not working - "sensor unavailable", so I checked the Kinect usb port on the computer and nothing abnormal is happening. I disconnected and reconnected the camera, nothing happens.

What to do?! Google search for < Kinect not responding >. Google suggested to try with different usb port. So we looked for another usb port with another Kinect. Oh¡ there’s a bip¡ it sounds like when my computer battery needs to get charged. I think maybe the usb has been detected. Ah¡ Gustavo saved us and shared with us the little trick that no one knows before using Kinect - that we simply needed to plug the usb BEFORE we open the Skanect software. And here it finally starts... Oh no again¡ Skanect tells us it did not find a source of data. We went to check the full instructions online and went back to google a second time. Gustavo let us his Macbook and there it worked properly. Clicked START and I walked around in a distance to capture the Object.

[![]({{site.baseurl}}/images/research_trip/vimeo-screenshot1.JPG)](https://vimeo.com/user85387954/review/336545784/e183b1d323)
Video 1 Skanect - timelapse of the scanning.

[![]({{site.baseurl}}/images/research_trip/vimeo-screenshot2.JPG)](https://vimeo.com/user85387954/review/336546468/a5d54f6cc2)
Video 2 Skanect - timelapse cleaning the scan.

[![]({{site.baseurl}}/images/research_trip/vimeo-screenshot3.JPG)](https://vimeo.com/user85387954/review/336546693/ee9c7413c1)
Video 3 Skanect - timelapse material render.

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/faf87bd27eac4f23aa0264d86905408a/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
Chair on Gustavo Abreu's Sketchfab


### 3D design
![]({{site.baseurl}}/RhinoPicFlipDots.jpeg)

I have drawn on Rhino coins that will flip within their structure and all together would form a board to presenting different colours depending on the surfaces and the orientations of each coins. In a smaller scale and a lot more coin like surfaces, it would make a similar result as the reflection of light on the wing of a butterfly. Originally I had planned to add some living bacterial dye onto the surfaces.

I have designed those on Rhino and then tested the model by 3D printing one coin and its base. IN the design I had given a space of 1mm in the indent where the stick had to turn. However this was not well designed and forgot it would print it attached to the hollow surface/indent. To correct it I should change the design and print those two pieces separately and then slide the coin in the structure to be able to flip. I still manages to turn the coin but naturally it ended up breaking it as it was twisting the ends of the coin.

For the printing of the coin, I have put little filling as it does not need to be strong. Because I wanted it to print as fast as possible I made most of the settings in function of the time. It still took about 20 min to print this small piece.

![]({{site.baseurl}}/FlipCoin1.jpeg)
![]({{site.baseurl}}/FlipCoin2.jpeg)

Here's the details on how I set up and 3D printed the orbit design, and I used the same method for the coin printing. I only have changed to a very low infill setup as it is just a test and doesn't need to be very strong also I have changed so that it does less details and less time to make it.

Here are the files of my design:
[Flip Dot Rhino file]({{site.baseurl}}/resources/FlipDot.3dm)

[Flip Dot bak]({{site.baseurl}}/resources/FlipDot.3dmbak)

After testing my design I wanted to have a bit more fun and learn a bit more in the settings of the machine by taking a more complex structure as an example.

### 3D printing
![]({{site.baseurl}}/3dprinting2.jpeg)

I tested a model shared on Thingiverse. It is a knotted orbit designed with the Earth as a sphere rolling inside it. I chose this one as it is a quite challenging shape that requires a bit more knowledge on certain settings and more importantly because it is a shape that doesn't allow it to be done in the CNC machine.

After downloading the files and uploading it on the IAAC cloud, I opened it in SLIC3R PRUSA Edition software only used for the PRUSA printer. For other 3D printers available in the fablab, such as ULTIMAKER2, we would usually use CURA as the software to prepare the file for printing. These software do equally the same job, they are called slicers.

![]({{site.baseurl}}/3dsliderviewMoment.jpeg)

For this specific design which has 98% of it's structure not touching the plate, I have to set up the right support for it to be printed properly. The angle of every curve will determine how hard it is for the machine to print and therefore how much support it requires. If the overhang angles are more than a certain degree there is no need for support, but in this case these angles are inferior to this degree. Extra layers were needed to give the right strength to such complex and thin structure. I also have set up the support to be everywhere as there are shapes on top of each other but not touching, it will allow support to be built in between those too.

![]({{site.baseurl}}/3dprinting1.jpeg)

While doing the settings I can view the object in 3D tab, preview tab or with 2D top view tab, and use the slider to see at every layer how it will be constructed and printed and to help see some errors or critical points that need a change in settings.

Other settings:
Infill 20% (in any case, 50% should be enough unless you need an object with a lot of strength to handle a lot of pressure or weight)
Parameter (wall) 3
Layer height 0.2mm (I could put 0.1 to make it smoother and have a better finish but it would take mush longer)
Initial layer height 0.3mm
Line width 0.4mm
Solid layers 4
Top layers 4 (more layers means it will look more smooth at the finish)
Bottom layers 4
Support pattern is zig zag with 4mm spacing.

Object dimensions 84 x 90mm
Printing time 8 hours 24 minutes

Once all the settings have been checked or changed, I can export the G-Code onto the SD Card for the PRUSA printer. Then it is pretty easy to send the work to start. I haven't changed the colour type of PLA, so it will be black. PLA is starch and sugar based, which makes it a biodegradable material.

### Learnings from 3D Printing
I have learned what variables I can play with to save time in the printing stage. And depending on the purpose of the object I'm printing, I need to adjust different things such as infills and numbers of walls to make sure its structure will be good for its use. Also the diversity of materials can allow me to have different textures and flexibility. I now am able to estimate how long something would take to print and plan my time in accordance to this.

![]({{site.baseurl}}/3DPrintsetup.JPG)

Because I have made an error in the designing of the flip coin, I was unable to make it flip. I needed to leave more space in between for it to flip but equally the 3D printer would have used the small space to put some support to print if nothing from the coin was touching the other structure. Knowing the way the machines works, I should have printed the two pieces separately and change the design to be able to lock the coin in the supporting structure.  

![]({{site.baseurl}}/DetailFlipCoinProblem.JPG)
![]({{site.baseurl}}/DetailGhostFlipCoinProblem.JPG)
![]({{site.baseurl}}/ExplodePartsFlipCoins.JPG)

### Additive or subtractive?
The two 3D printing test I have made are both examples of something that could not have been made in a subtractive way as otherwise it would require at least a cnc with 5 or 7 axis and even with that the level of precision for such small pieces might not have been good enough for it to work. The end mill could also not be able to move inside the piece because of it size. That why 3D printing is a tool that can be very good for complex, small or precise structure.


## Possible future experiments
### 3D print nature-inspired textures
What if I can represent different state of an element? (ex. smoke printed 3D)
3D scan organic textures to 3D print it and challenge the haptic experience between the real and artificial. I haven't been able to do this yet as the Kinect only scans object of a certain size and capturing the texture would have to look at a much shorter distance.

[3D scanning micro objects](http://calculatedimages.blogspot.com/2013/07/micro-3d-scanning-1-focal-depth.html)

### 3D print using bio materials
I would like to use this week's assignments to start experimenting with the use of biomaterials for 3d printing. I was thinking maybe to 3D print an object using a bacteria medium as a material to print and later progressively grow a bacterial dye on this structure. I will have to discuss with the staff if this can be an option. It will depend on the fluidity and smoothness of the printing material.

[3D printing of bacteria into functional complex materials](http://advances.sciencemag.org/content/3/12/eaao6804)

[3D Bacterial Printing](https://pubs.acs.org/doi/pdf/10.1021/acssynbio.6b00395)

[Bacterial Culture Media Recipes](http://www.thelabrat.com/protocols/bacculture.shtml)

-------------

### Inspirations and examples for testing

[Parametric design terrain](https://www.youtube.com/watch?v=2TM27LXXVgg)

[Knotted orbit](https://www.thingiverse.com/thing:512210)

[RCS Loopie](https://www.thingiverse.com/thing:272580)

[Chain mail trimesh](https://www.thingiverse.com/thing:2611564)
