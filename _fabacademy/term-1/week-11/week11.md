---
title: 11 | Output devices
period: 20 March 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/)

## Output Devices
I have chosen as my output device a servo motor. I am taking the board traces and outline from Neil's boards on the Fab Academy [page](http://academy.cba.mit.edu/classes/output_devices/index.html).

### Board design
Because I wanted to use a light sensor and have it work with the servo motor, as described in the input devices assignment, I had to redesign the servo motor board to add the light sensor. This mean I had to add the light sensor, and a resistor (10K). To connect the sensor to the data pin correctly it is important to check that the pin I choose to connect to on the servo motor has a analogue to digital converter (ADC). I was shown online the Attiny 44 and Attiny 45 pin out to compare.


![]({{site.baseurl}}/hellolight45Components.png)
![]({{site.baseurl}}/helloservo44Components.png)

I used photoshop to do this merge, by simply copying and pasting pins and bits of lines to make sure it was the right dimension.

![]({{site.baseurl}}/ServoLightNEWfixed.png)
![]({{site.baseurl}}/ServoLightNEWfixedOutline2.png)

![]({{site.baseurl}}/PSDservolight.jpeg)
![]({{site.baseurl}}/PSDServoLightAdded.jpeg)


### Mill board
![]({{site.baseurl}}/hellolight44traces.png)
![]({{site.baseurl}}/HelloServo44Traces.png)

I ran into small issue. What happened is that when I exported from photoshop the pngs, I didn't not check the size it had. I did the same mistake as in the input milling  but this time working on a bigger board on photoshop and then checked the export size which is about 2000 x 2000. This will be too big too. So I have exported from Josep file the images and are now the correct size of 1776 x 1714.

Another small other problem I got is when I tried to mill the outline, a small window popped up to explain that it didn't want to mill as something in the code was not right. I remade the file from Fab Modules with this time changing the xhome: 0, yhome: 0, zhome: 10 to make see if this was the problem, as maybe it needed to go out of the cutting area. After this it worked.

And then a third problem I encountered was again a scale issue but this time milled a board that was far to big. The issue was when I had exported, the dpi was too high and it showed also in Fab modules when I set up the file but didn't pay attention. The dpi was at 72000 but should have been at 1000 for my document. To correct this mistake, I was shown by the instructor onto photoshop how to make sure that when I exported it was the right dpi and saved it at the right pixels by pixels dimensions.

![]({{site.baseurl}}/MillingOutput.jpg)

I finally milled the board in the right dimensions.

### Solder board
![]({{site.baseurl}}/hellolight45Components.png)
![]({{site.baseurl}}/helloservo44Components.png)

![]({{site.baseurl}}/ComponentsSolderOutput.jpg)
![]({{site.baseurl}}/FINALoutputPicture.jpg)

I had to ask for some help to find the right components because what was indicated on the board and the picture was not enough for me to find them in the lab and it after looking for the ones I didn't know for a while I decided to ask.

The components I soldered are as follow:
- (1) CAP CER 10UF
- (2) 10K resistor
- (1) Microprocessor ATTINY 44
- (3) FTDI Header (of which one I cut two legs off)
- (1) Ceramic resonator 20MHZ
- (1) IC2 Regulator 1A (checked which one of the two by looking at Neil's picture of the board)
- (1) LED green clear

To solder the board was good. The only thing is that the components for the servo part of the board were much closer to each other and so not always easy to solder, which makes mistake in the soldering much easier to do.

![]({{site.baseurl}}/connection-outpu-servo.jpg)

### Programming of the board

I encountered problems form the begining which were solved with help from the instructor. The first is that I tried to program this output device with a ISP board, battery, small servo motor and a bigger one, however the microcontroller immediately started getting burning hot. What happened is that the current the ISP was giving was going against the current the of the input device. To solve this problem, we plugged a battery to my board to balance against the power that is being given by the ISP board.
Another problem encountered was that the servo8bit downloaded from the library is not working, therefore I had to find another code online for the servo that was not in the library. We looked for Arduino servo with no library code and found a software servo library that I downloaded and then looked for in the library manager a to use in Arduino software. In order to know which pin number to use for servos and tight transistor, I searched for the pin out of the six pins and find the ones that correspond to where I have plugged my servo to.

Next

![]({{site.baseurl}}/ArduinoProgrammingSweepServo.JPG)
![]({{site.baseurl}}/ServoLibraryAdded.JPG)
![]({{site.baseurl}}/TrinketKnobAdafruitCodeFail.JPG)
![]({{site.baseurl}}/TrinketKnobAdafruitCodeFail2.JPG)


### Files
[Photoshop design file]({{site.baseurl}}/resources/ServoLight.psd)
[Outline new board]({{site.baseurl}}/resources/ServoLightNEWfixedOutline2.rml)
[Traces new board]({{site.baseurl}}/resources/ServoLightNEWfixed.rml)

[Hello board servo 44 motor - outline]({{site.baseurl}}/resources/helloServo44Outline.rml)
[Hello board servo 44 motor - traces]({{site.baseurl}}/resources/HelloServo44Traces.rml)

### Useful Links
http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html

http://highlowtech.org/?p=1695

https://forum.arduino.cc/index.php?topic=62912.0

https://www.youtube.com/watch?v=TRung0wdH78
