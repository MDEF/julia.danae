---
title: 14 | Networking and Communications
period: 20 March 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

For this week’s assignment, I have teamed up with Jules and Maite in order to be more time efficient. We’ve decided to use this detailed [document](https://hackmd.io/s/B15x5dn9V#HC08-Communication) to follow every step in the process.

We started by connecting the Bluetooth to the Arduino to the computer. To connect properly we looked at the small writing on the board and made sure to connect it equally.

![]({{site.baseurl}}/arduino-ble-hc08.jpg)

When starting we struggled with connecting the Arduino board to Maite’s computer so we used my computer and Arduino. The issue is that we will need both Arduino functioning on both computers.
Started by uploading a blank sketch and then opened Tools to select the port > serial monitor > AT > enter. The board was not found.

We followed the Example -1 from the document to start.

{% highlight ruby %}
#include <SoftwareSerial.h>
SoftwareSerial BT1(10, 11); // RX | TX
void setup()
  { pinMode(8, OUTPUT);        // Al poner en HIGH forzaremos el modo AT
    pinMode(9, OUTPUT);        // cuando se alimente de aqui
    digitalWrite(9, HIGH);
    delay (500) ;              // Espera antes de encender el modulo
    Serial.begin(9600);
    Serial.println("Levantando el modulo HC-06");
    digitalWrite (8, HIGH);    //Enciende el modulo
    Serial.println("Esperando comandos AT:");
    BT1.begin(38400);
  }

void loop()
  {  if (BT1.available())
           Serial.write(BT1.read());
     if (Serial.available())
        BT1.write(Serial.read());
  }
{% endhighlight %}

We change the serial value from 9600 to 1200pbs doesn’t work, so we swapped the RXD to 0 and TXD to 1 to see if it can work. The default RX and TX is not working.

We’ve found the problem after multiple testing. The problem was that when uploading a blank sketch and the Bluetooth is connected it doesn’t work but when we upload first the sketch and then connect the Bluetooth it works.

Once it works we typed *AT+CMODE=1 (master mode)* to communicate with it but it doesn’t respond.
We are using software serial which is like a library of pins. So we define the pins used for the software serial, which will be 10 and 11 in this case.

Two types of communication happening, A going to computer and Arduino (9600pbs) and B Arduino and Bluetooth (38400pbs). Problem is type B is not working. The Bluetooth didn’t have the same order as the cable so we plugged individually each of the connections, following the picture under here.

![]({{site.baseurl}}/GM-TTL5VT-UART-Pinout.jpg)

Now we follow the datasheet on the documentation called *AT command list*. We’ve written two of the commands: AT and AT+VERSION.

Now we have to set up the master by putting the code *AT=CMODE=1*. Next we type *AT+ADDR* but it doesn’t seem to work cause we didn’t get an address. So we tried to write *AT+PINXXXX*, not working neither.

![]({{site.baseurl}}/BLUETOOTH-ATCOMMUNICATION.JPG)

So after starting several times from the beginning, we finally realised that the problem was simply a space between AT and the +. Now it is responding but not the address. We’ve been told that we should probably try with an ? at the end of the command because it is not an official board. Now it is working, we have the address: 587A624FE6FD master address.

![]({{site.baseurl}}/CablesConnectionsBluetooth.jpg)
![]({{site.baseurl}}/CablesConnectionsBluetooth2.jpg)

Next we need to get another Bluetooth chip to then make it a slave.

Then we’ll have to make the communication between the two.
...


### Code


### Useful links

[Instructables Bluetooth module](https://www.instructables.com/id/AT-command-mode-of-HC-05-Bluetooth-module/)

[](https://hackmd.io/s/B15x5dn9V#HC08-Communication)
