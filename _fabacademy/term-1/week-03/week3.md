---
title: 3 | Computer control cutting
period: 6 February 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

This week we are learning about computer controlled cutting, which includes using laser and vinyl cutters.

# Laser cutting
## Steps before cutting
It is not the first time I use the laser cutting the Fab Lab. I import my rhino file or DWG file in rhino on the local computer, and then make sure the layer for cutting is the right colour and the layer for rastering is the right colour too. I then write the command print to set up the speed, power and thickness for the correct material. In this case I was using a 3mm cardboard. Once these are set up I can select my file and place it and scale it according to the machine size and material size.

I then adjust the distance between the material surface and the laser tip so that it is focused and won't burn a thicker line, and tape the corners if it is bending a little in some place. Once this is checked I can then turn on the extractor and send my file to be cut and keep an eye on the machine as long as it is running. Once done I make sure to clean the surface for the next person to use.

The cutting was done on a 3mm cardboard, with 80 power and 100 speed at 1000 Hz as an engraving set up.


# Individual assignments
I have created a small press-fit (1) of a landscape in cardboard and have tested the living hinges (2) in laser cut. All the details can be found below.

## Press fit
### 1

![]({{site.baseurl}}/PressFitMaking.jpeg)

I have decided to use Illustrator software to design the pieces for the press fit. I just wanted to create a grid of landscape with changing heights and test how it would fit together. I didn't spend too much tie on the design part as I just wanted to see first what might be a problem and what worked. To make the indents inside the surfaces of the pieces, I have increased the large thickness to the thickness of my material.

![]({{site.baseurl}}/RhinoPressfit.JPG)

![]({{site.baseurl}}/image00035.jpeg)

![]({{site.baseurl}}/image00014.jpeg)


### Parametric design

I have forgotten that it had to be a parametric design for the laser cutting, which makes more sense as that can be easily readjusted following some testing it, to correct possible errors such as the width of cut for press fit that is too large or too thin. So after having already tested a press fit I have made with illustrator, I used Grasshopper from Rhino6 to create a parametric design.

I have two choices, either make something I find interesting and could be useful for my project, but will take longer, or make something basic simple to understand the basics of Grasshopper and laser cut it for testing.

Because I have already tested the laser cutting, but do not know much about Grasshopper, I will make a simple design to learn the basics.

### Steps of script explained:

![]({{site.baseurl}}/ParametricDesignPolygoneStep1.JPG)

I started by double clicking on Grasshopper page and typed *Polygon*. It automatically created one in Rhino as it also created an element in Grasshopper. To this element I connected a Point I created and placed in the centre of the shape. I then created sliders, one for the *Radius*, another for the *Segments* and the third for the *Fillet Radius*. To create a slider rapidly, I found the trick to directly write the min, max and the number where you want it to be in between, by double clicking in grasshopper and directly wrote *1<10<30* (for instance) for everyone of them. This can be change again later if needed by double clicking on the slider element. I then can choose whatever shape I am happy to work with, and styed with a polygon of 7 segments, 3 radius and 0 in fillet radius.

Now I need to create a joint on every segment of the polygon I have created. I started by creating a *Rectangle* with a point at the centre of the polygon, however it is not the best starting point to create the shortest and easiest way to make it. So, with the advice of the instructor, I continued by dividing the segments that form the shape. *Explode* is the command used to do that. Then we need to find the centre point of those segments, to do that we use the command *Evaluate Curve* and connect it to a panel that will define the parameter. Because we are trying to find the middle point, we look for the half of the distance: *0.5*. Next step was to *Construct Planes* at each of this middle points in order to manipulate the orientation of the planes we are adding later. The origins of each as each middle  point and tangent as x-axis. Then I can create a *Rectangle* at each of these planes. The dimensions on plane x of rectangle will be defined by a *Domain* that I add now. This will end with whatever number I put for the *Number Slider* and start at a *Negative* of that number. I do this too to define the *Y axis* dimensions. Now that the rectangles position and sizes are defined, the last step is to do a Boolean called a *Region Difference*. Curve A is my polygon and curve B are my rectangles that I created.

 ![]({{site.baseurl}}/PressFitGrasshopperRegionDiff.JPG)


 Note that the purple frames are other options for doing the same result however the script below is not finished and unnecessarily more complicated. It the way I started doing at the beginning but continued a bit further. In this case the domains for X and Y are put directly on the rectangle in the centre.

 ![]({{site.baseurl}}/PressFitGrasshopperOption2Unfinished.JPG)


### 2

![]({{site.baseurl}}/LivingHingeRhino.JPG)
![]({{site.baseurl}}/LivingHingeRhino2.JPG)

I first designed the living hinges on Rhino. I choose the types of living hinges I wanted to test out of examples I had seen and then created from scratch the different boards. I wanted to test the terrain one as Î thought the result could be interestingly unusual.
However I failed to properly be able to test those because when I laser cut them I set up the file in the wrong dimensions and they all ended up very small which made it loose its resistance and flexibility because the space between lines reduced in size considerably too.

![]({{site.baseurl}}/screencapturelivingingetests.jpeg)

I have decided to use Illustrator to draw the press fit model. I have drawn a basic square based landscape that would slide in each other to create a curvy surface.


# Group assignment
We tested different methods for laser cutters and checked their settings and try understand how they work in order to get better results. We tested with 3mm and 5mm cardboard.


## Living hinge test
I tested the living hinge on 3mm cardboard and different versions I have drawn in Rhino. I wanted to use scrap material from the Fablab and didn't want to use too much material for the testing. The scale at which I printed it was too small for the living hinge to be solid enough but demonstrated the model drawn.

![]({{site.baseurl}}/image00016.jpeg)

Living hinge design worked to make the material flexible but the lines were too close to each other and ended up breaking after bending several times.

![]({{site.baseurl}}/image00013.jpeg)
![]({{site.baseurl}}/image00011.jpeg)

This hinge worked pretty well and was more flexible than I thought it would be. However, once again the line were to close to each other.

![]({{site.baseurl}}/image00010.jpeg)
![]({{site.baseurl}}/image00021.jpeg)
![]({{site.baseurl}}/image00018.jpeg)

I tried to design three types of living hinge and experimented with other kind of living hinge. The lines were too close to each other and I did the mistake to make the border too narrow and therefore it can break to easily when bent.

![]({{site.baseurl}}/image00009.jpeg)
In the end, the tests were quite inspiring for future texture or experimentation with surfaces.


# Vinyl cutting
The vinyl cutter which is using a adhesive plastic surface which can be used for stickers or screen print. I have used one of my graphic work made in Illustrator with vectors, and then imported it in Rhino to clean again the file and prepare for the vinyl cutter.

![]({{site.baseurl}}/CaptureTateIllustrator.JPG)

The Vinyl cutter available in the Fab lab is a Roland GS24 Camm-1 and used CutStudio software to prepare the file. To start I have put the material I had pre-cut into the machine and placed the white pieces, to hold the material in place, at the same level as the blue marks on the machine. This will prevent from the material moving while cutting. Once turned on, I pressed the button Get and the machine measured the surface that can be cut on the surface.
I first cute a small circle in the corner of my material as a test to see if it works well. Once done, I got back to the program and send the job to the machine as follows: File > Cutting Setup > Properties > Get  (button) and click ok.

![]({{site.baseurl}}/image00033.jpeg)
![]({{site.baseurl}}/image00034.jpeg)
![]({{site.baseurl}}/image00012.jpeg)
![]({{site.baseurl}}/image00015.jpeg)

To separate the sticker from the other sheet properly, I used masking tape to put on top of my design to make sure all the intricate design stay together and don't change of shape as I unstick and stick them onto another surface. I have used the result as a sticker for my sketchbook.

![]({{site.baseurl}}/FinalVinylCutSticker.jpg)


### Files for vinyl cut and laser cut

1.
[Press Fit Test Land on Rhino]({{site.baseurl}}/resources/PressFitTestLandRhino.dwg)

[Press Fit Test Land on Illustrator]({{site.baseurl}}/resources/PressFitTestLandIllustrator.ai)

2.
[Living Hinge test Rhino]({{site.baseurl}}/resources/LivingHinge test.3dm)

[Living Hinge test Illustrator]({{site.baseurl}}/resources/LivingHinge test.AI)

3.
[Graphic for vinyl cut on Rhino]({{site.baseurl}}/resources/TateModernPosterLaserCutv2.3dm)

[Tate Print Design Illustrator]({{site.baseurl}}/resources/TatePrintA3.ai)


----------------
**Raster Engraving Test on Acrylic**
Veronica's documentation is [HERE](https://mdef.gitlab.io/veronica.tran/fab-academy/week-03/)

**Press Fit Test on cardboard**
Alexandre's documentation is [HERE](https://mdef.gitlab.io/alexandre.acsensi/fabacademy/w3computercontrolledcutting.html)

**Kerf Test on plywood**
Tom's documentation is [HERE](https://mdef.gitlab.io/thomas.barnes/fabacademy/Computer_controlled_cutting/)
